import axios from 'axios';
import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

var subToTeamName = {
  'r/denvernuggets' : 'Nuggets',
  'r/mavericks' : 'Mavericks',
  'r/warriors' : 'Warriors',
  'r/rockets' : 'Rockets',
  'r/laclippers' : 'Clippers',
  'r/memphisgrizzlies' : 'Grizzlies',
  'r/timberwolves' : 'Timberwolves',
  'r/nolapelicans' : 'Pelicans',
  'r/thunder' : 'Thunder',
  'r/suns' : 'Suns',
  'r/ripcity' : 'Blazers',
  'r/kings' : 'Kings',
  'r/nbaspurs' : 'Spurs',
  'r/utahjazz' : 'Jazz',
  'r/atlantahawks' : 'Hawks',
  'r/bostonceltics' : 'Celtics',
  'r/gonets' : 'Nets',
  'r/charlottehornets' : 'Hornets',
  'r/chicagobulls' : 'Bulls',
  'r/clevelandcavs' : 'Cavs',
  'r/detroitpistons' : 'Pistons',
  'r/pacers' : 'Pacers',
  'r/heat' : 'Heat',
  'r/mkebucks' : 'Bucks',
  'r/nyknicks' : 'Knicks',
  'r/orlandomagic' : 'Magic',
  'r/sixers' : 'Sixers',
  'r/torontoraptors' : 'Raptors',
  'r/washingtonwizards' : 'Wizards',
  'r/lakers' : 'Lakers',
}

export default class RankingTable extends React.Component {
  state = { data: [] };
  componentDidMount(){
    axios.get('/api/teams')
      .then(response => {
        this.setState({ data: response.data});
      })
      .catch(function (error) {
        console.log(error);
      })
  }
  render() {
    const { data } = this.state;
    return (
      <div>
        <br />
        <h3>Subscribers</h3>
        <br />
        <ReactTable
          data={data}
          columns={[
            {
              Header: "#",
              id: "row",
              maxWidth: 50,
              filterable: false,
              sortable: false,
              Cell: (row) => {
                return <div>{row.index+1}</div>
              }
            },
            {
              Header: "Subreddit",
              accessor: "subreddit",
              minWidth: 110,
              maxWidth: 130,
              sortable: false,
              style: { textAlign: "left"},
              Cell: row => (
                <div>{subToTeamName[row.value]}</div>
              )
            },
            {
              Header: "Subscribers",
              accessor: "subscribers",
              minWidth: 100
            },
            {
              Header: "last week",
              sortMethod: (a, b) => {
                if (a && b) {
                  if (a.length === b.length) {
                    return a > b ? 1 : -1;
                  }
                  return a.length > b.length ? 1 : -1;
                }
              },
              accessor: "changeWeek",
              Cell: row =>(
                <div
                  style={{
                    width: "100%",
                    height: "100%",
                    borderRadius: "6px",
                    backgroundColor:
                      row.value > this.state.data.map(o => Number(o.changeWeek)).sort((a,b) => a-b)[21]
                        ? "#2dcc40"
                        : row.value > this.state.data.map(o => Number(o.changeWeek)).sort((a,b) => a-b)[13]
                        ? "#97F500"
                        : row.value > this.state.data.map(o => Number(o.changeWeek)).sort((a,b) => a-b)[5]
                        ? "#ffdc00"
                        : "#f48e34",
                  }}
                >
                  {row.value}
                </div>
              ),
            },
            {
              Header: "last month",
              sortMethod: (a, b) => {
                if (a.length === b.length) {
                  return a > b ? 1 : -1;
                }
                return a.length > b.length ? 1 : -1;
              },
              accessor: "changeMonth",
              Cell: row =>(
                <div
                  style={{
                    width: "100%",
                    height: "100%",
                    borderRadius: "6px",
                    backgroundColor:
                      row.value > this.state.data.map(o => Number(o.changeMonth)).sort((a,b) => a-b)[21]
                        ? "#2dcc40"
                        : row.value > this.state.data.map(o => Number(o.changeMonth)).sort((a,b) => a-b)[13]
                        ? "#97F500"
                        : row.value > this.state.data.map(o => Number(o.changeMonth)).sort((a,b) => a-b)[5]
                        ? "#ffdc00"
                        : "#f48e34",
                  }}
                >
                  {row.value}
                </div>
              ),
            }
          ]}
          defaultPageSize={30}
          defaultSorted={[
              {
                id: "subscribers",
                desc: true
              }
            ]}
          showPagination={false}
          resizable={false}
          className="-highlight"
        />
        <br />
        <i>Updated every hour</i>
        <br />
        <br />
      </div>
    );
  }
}

