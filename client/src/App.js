import React, { Component } from 'react';
import RankingTable from './components/RankingTable';
import NavBar from './components/NavBar';

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="NavBar">
          <NavBar />
        </div>
        <div className="RankingTable">
          <RankingTable />
        </div>
      </div>
    );
  }
}

export default App;
