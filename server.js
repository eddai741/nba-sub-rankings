require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const schedule = require('node-schedule');

const teams = require('./routes/api/teams');
const updateSubscribers = require('./tasks/update-subscribers');
const addToArchive = require('./tasks/add-to-archive');

const app = express();

app.use(bodyParser.json());

// Connect to Mongo
const db = process.env.mongoURI;
mongoose
	.connect(db, {useNewUrlParser: true})
	.then(() => console.log('MongoDB connected'))
	.catch(err => console.log(err));

// Updates every team's number of subcribers every hour at XX:00
schedule.scheduleJob('0 * * * *', updateSubscribers);

// Saves the current state to the archives once a day at 01:01 UTC
schedule.scheduleJob('1 1 * * *', addToArchive);

// Use Routes
app.use('/api/teams', teams);

// Serve static assets if in production
if(process.env.NODE_ENV === 'production') {
	// Set static folder
	app.use(express.static('client/build'));

	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
	});
}


const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on ${port}`));
