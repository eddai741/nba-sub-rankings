const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Document for storing the latest subscriber count for
// each subreddit. This is shown on the website
const TeamSchema = new Schema({
	subreddit: {
		type: String
	},
	subscribers: {
		type: Number
	}
});

module.exports = Team = mongoose.model('team', TeamSchema);
