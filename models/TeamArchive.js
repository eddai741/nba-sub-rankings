const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Document for storing historic subscriber data
const TeamArchiveSchema = new Schema({
	date: {
		type: String
	},
	subreddit: {
		type: String
	},
	subscribers: {
		type: Number
	}
});

module.exports = TeamArchive = mongoose.model('teamArchive', TeamArchiveSchema);
