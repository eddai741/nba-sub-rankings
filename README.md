# NBA Subreddit Rankings

This is a website for ranking NBA team subreddits by subscriber count and growth.

Website is live at www.nbasubrank.xyz.

## Installation

In root directory (may require sudo):

```bash
npm install
cd client
npm install
```

You will need to configure your own MongoDB deployment. After doing this,
create a .env file in the root directory and set mongoURI variable
to your MongoDB URI. 
```bash
mongoURI = <MongoDB_URI>
```

## Usage

To run locally, in root directory:

```bash
npm run dev
```

To deploy on Heroku:

Follow instructions on https://devcenter.heroku.com/articles/git




## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.