require('dotenv').config();
const snoowrap = require('snoowrap');

const TeamArchive = require('../models/TeamArchive');

var subredditNames = ['mavericks', 'denvernuggets', 'warriors', 'rockets', 'laclippers',
		      'lakers', 'memphisgrizzlies', 'timberwolves', 'nolapelicans',
		      'thunder', 'suns', 'ripcity', 'kings', 'nbaspurs', 'utahjazz',
		      'atlantahawks', 'bostonceltics', 'gonets', 'charlottehornets',
		      'chicagobulls', 'clevelandcavs', 'detroitpistons', 'pacers',
		      'heat', 'mkebucks', 'nyknicks', 'orlandomagic', 'sixers',
		      'torontoraptors', 'washingtonwizards']

module.exports = () => {

  // Reddit API
  // Create a new snoowrap requester with OAuth credentials.
  const r = new snoowrap({
    userAgent: 'my app',
    clientId: process.env.clientId,
    clientSecret: process.env.clientSecret,
    username: process.env.username,
    password: process.env.password
  });

  // Create string for date storage in archive
  // Dates are compared using string comparison
  var today = new Date();
  today = today.toDateString();

  // Saves the current state of subscribers for every team to the archives
  subredditNames.forEach((element) => {
    r.getSubreddit(element).fetch()
      .then(response => {
	TeamArchive.create({
	  date : today,
	  subreddit : 'r/' + element,
	  subscribers : response.subscribers
	})

      })
  });
}
