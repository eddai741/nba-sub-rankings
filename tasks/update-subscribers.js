require('dotenv').config();
const snoowrap = require('snoowrap');

const Team = require('../models/Team');

var subredditNames = ['mavericks', 'denvernuggets', 'warriors', 'rockets', 'laclippers',
		      'lakers', 'memphisgrizzlies', 'timberwolves', 'nolapelicans',
		      'thunder', 'suns', 'ripcity', 'kings', 'nbaspurs', 'utahjazz',
		      'atlantahawks', 'bostonceltics', 'gonets', 'charlottehornets',
		      'chicagobulls', 'clevelandcavs', 'detroitpistons', 'pacers',
		      'heat', 'mkebucks', 'nyknicks', 'orlandomagic', 'sixers',
		      'torontoraptors', 'washingtonwizards']

module.exports = () => {

  // Create a new snoowrap requester with OAuth credentials.
  const r = new snoowrap({
    userAgent: 'my app',
    clientId: process.env.clientId,
    clientSecret: process.env.clientSecret,
    username: process.env.username,
    password: process.env.password
  });

  // Update each Team document with recent data
  subredditNames.forEach((element) => {
    r.getSubreddit(element).fetch()
      .then(response => {
	Team.updateOne({ subreddit : 'r/' + element}, { subscribers : response.subscribers })
	  .then(result => {
	    if (result.n == 0) {
	      Team.create({
		subreddit : 'r/' + element,
		subscribers : response.subscribers
	      });
	    }
	  })
	});
  });
}
