const memoryCache = require('memory-cache');
const express = require('express');
const path = require('path');
const router = express.Router();

const Team = require('../../models/Team');
const TeamArchive = require('../../models/TeamArchive');

// Cache for request
var cache = (duration) => {
  return (req, res, next) => {
    let key = '__express__' + req.originalUrl || req.url
    let cachedBody = memoryCache.get(key)
    if (cachedBody) {
      res.send(cachedBody)
      return
    } else {
      res.sendResponse = res.send
      res.send = (body) => {
        memoryCache.put(key, body, duration * 1000);
        res.sendResponse(body)
      }
      next()
    }
  }
}



// Compares current and historic JSON objects and extends
// current JSON object with a new attribute
var extend = (current, historic, newAttribute) => {
  // Match subreddit name
  for (var i = 0; i < current.length; i++) {
    for (var y = 0; y < historic.length; y++) {
      if (current[i].subreddit == historic[y].subreddit) {
        // This is a mess but performance is OK
        try {
          current[i] = current[i].toJSON();
        }
        catch(error){
          console.log(error)
        }
        // Extend 'current' JSON with new attribute to show change in subscribers
        // change in subs = current - historic
        var change = current[i].subscribers - historic[y].subscribers;
        current[i][newAttribute] = (change<=0?"":"+") + change;
      }
    }
  }
  return current;
}


router.get('/', cache(300), (req, res) => {
  // Get current team data
  Team.find().sort({'subscribers' : -1}).then(currentData => {
      var today = new Date();
      today.setDate(today.getDate() - 7);
      dayWeekAgo = today.toDateString();
      // Get team data from 7 days ago
      TeamArchive.find({
        date: dayWeekAgo
      }).then(weekAgoData => {
          var today = new Date();
          today.setDate(today.getDate() - 30);
          dayMonthAgo = today.toDateString();
          // Get team data from 30 days ago
          TeamArchive.find({
            date: dayMonthAgo
          }).then(monthAgoData => {
            // add changeMonth and changeWeek fields to JSON
            var newJson = extend(currentData, monthAgoData, "changeMonth");
            var output = extend(newJson, weekAgoData, "changeWeek");
            res.json(output)
          })
        })
    });
});



module.exports = router;
